class Arithmatics {
    number1: number;
    number2: number;

    constructor(no1: number, no2: number) {
        this.number1 = no1;
        this.number2 = no2;
    }
    addition(): number {
        return this.number1 + this.number2;
    }

    sustraction(): number {
        return this.number1 - this.number2;
    }

    multiplication(): number {
        return this.number1 * this.number2;
    }

    division(): number {
        return this.number1 / this.number2;
    }

}

var arithmatics1 = new Arithmatics(25,5);
var arithmatics2 = new Arithmatics(200,35);

console.log("Obj1 Addition "+ arithmatics1.addition());  
console.log("Obj1 Substraction "+ arithmatics1.sustraction());  
console.log("Obj1 Multiplication "+ arithmatics1.multiplication());  
console.log("Obj1 Division "+ arithmatics1.division());  

console.log("No Second Object Calculations");

console.log("Obj1 Addition "+ arithmatics2.addition());  
console.log("Obj1 Substraction "+ arithmatics2.sustraction());  
console.log("Obj1 Multiplication "+ arithmatics2.multiplication());  
console.log("Obj1 Division "+ arithmatics2.division()); 
