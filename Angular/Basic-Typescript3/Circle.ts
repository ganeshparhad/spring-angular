class Circle {
    PI: number;
    redius: number;

    constructor(pi: number, redius: number) {
        this.PI = pi;
        this.redius = redius;
    }

    area(): number {
        return this.PI * this.redius * this.redius;
    }
}
var PI = 3.14;
var circle1: new Circle(PI, 5);

var circle2: new Circle(PI, 10);

console.log("1st circle"+ circle1.area());