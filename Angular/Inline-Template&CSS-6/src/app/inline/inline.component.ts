import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inline',
  template: `
    <h1><p>
      inline works!
    </p></h1>
  `,
  styles: [`
  p{
    color:red
  }`]
})
export class InlineComponent {

}
