package com.amdocs.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootTraining21Application {

	public static void main(String[] args) {
		SpringApplication.run(BootTraining21Application.class, args);
	}

}
