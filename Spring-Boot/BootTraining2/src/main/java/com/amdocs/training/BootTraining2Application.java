package com.amdocs.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootTraining2Application {

	public static void main(String[] args) {
		SpringApplication.run(BootTraining2Application.class, args);
	}

}
