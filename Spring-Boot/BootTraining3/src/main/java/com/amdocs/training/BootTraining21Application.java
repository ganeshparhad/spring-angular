package com.amdocs.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
@ComponentScan
public class BootTraining21Application {

	public static void main(String[] args) {
		SpringApplication.run(BootTraining21Application.class, args);
	}

}
