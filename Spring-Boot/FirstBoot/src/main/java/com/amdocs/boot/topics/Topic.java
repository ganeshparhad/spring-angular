package com.amdocs.boot.topics;

import lombok.Data;

@Data
public class Topic {
	
	private String id;
	private String name;

}
 