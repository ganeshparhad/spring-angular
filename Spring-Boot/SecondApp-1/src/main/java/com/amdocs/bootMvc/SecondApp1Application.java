package com.amdocs.bootMvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecondApp1Application {

	public static void main(String[] args) {
		SpringApplication.run(SecondApp1Application.class, args);
	}

}
